import { html, css, LitElement } from 'lit-element';

export class Ejercicio1 extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
      }
    `;
  }

  static get properties() {
    return {
      name: { type: String },
      count: { type: Number },
      arr: { type: Array },
      myBool: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.name = 'Mundo';
    this.count = 0;
    this.arr = [
      'azul',
      'cafe',
      'rojo',
      'violeta',
      'verde',
      'negro'
    ];
    this.myBool = false;
    this.alumnos = [
      {name: 'Pedro', edad: 23},
      {name: 'Antonio', edad: 33},
      {name: 'Juan', edad: 10},
      {name: 'Julio', edad: 67},
      {name: 'Octavio', edad: 22},
      {name: 'Maria', edad: 26},
      {name: 'Julia', edad: 25},
      {name: 'Noelia', edad: 44},
      {name: 'Noe', edad: 30},
      {name: 'Eduardo', edad: 26},
      {name: 'Javier', edad: 29},
      {name: 'Nicolas', edad: 44}
  ];

  //PINTAR EN EL RENDER, LOS ALUMNOS QUE TENGAN 25 AÑOS O MAS Y 30 AÑOS O MENOS
  //UTILIZAR FUNCIONES PROPIAS DE ARRAYS
  //PLASMAR EN EL RENDER DE LA MANERA QUE PREFIERAN (ETIQUETAS)
  }

  counter() {
    this.count++
    this.name = 'Saludo';
    // this.count += 1
    // this.count = this.count + 1
    this.myBool = !this.myBool;
  }

  render() {
    return html`
      <h1>Hello, ${this.name}</h1>
      <button @click="${this.counter}">CONTADOR DE BOTON ${this.count}</button>
      <ul>
        ${this.arr.map(i => html`<li>${i}</li>`)}
      </ul>
      ${this.myBool ?
        html`<p>Render some html if myBool is true</p>` :
        html`<p>Render other html if myBool is false</p>`}
        <ul>
          <h3>alumnos de 25 años o más</h3>
          ${this.alumnos.map(i => html`<li><strong>${i.name}</strong>${i.edad >= 25}</li>`)}
        </ul>
        <ul>
          <h3>alumnos de 30 años o menos</h3>
          ${this.alumnos.map(i => html`<li><strong>${i.name}</strong>${i.edad <= 30}</li>`)}
        </ul>
    `;
  }
}

customElements.define('ejercicio-1', Ejercicio1);