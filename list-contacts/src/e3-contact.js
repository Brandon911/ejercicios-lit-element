import { html, css, LitElement } from 'lit-element'

export class e3Contact extends LitElement {
  static get styles() {
    return css`
      div {
        border: 1px solid black;
        padding: 10px;
        border-radius: 5px;
        display: inline-block;
      }
      h1 {
        font-size: 1.2rem;
        font-weight: normal
      }
    `;
  }


  static get properties() {
    return {
      genero: { type: String },
      seeMore: { type: Boolean },
      option: { type: String }
    }
  };

  constructor() {
    super();
    this.genero = '';
    this.seeMore = false;
    this.option = 'Ver Mas';
  }

  toggle() {
    this.seeMore = !this.seeMore;
    this.seeMore ? this.option = 'Ver menos' : this.option = 'Ver Mas'
  }

  render() {
    return html`
     <div>
      <h1>${this.genero}</h1>
      <p><a href="#" @click="${this.toggle}">${this.option}</a></p>

      ${this.seeMore ? html`genero: ${this.genero}` : ''}
    </div>
    `;
  }
}

customElements.define('e3-contact', e3Contact);