import { html, LitElement } from 'lit-element';
import './e-contact.js';
import './e2-contact';
import './e3-contact';



export class ListContact extends LitElement {

  static get properties() {
    return {
      contacts: { type: Array }
      //IMPORTAR 2 COMPONENTES APARTE DEL PRINCIPAL
      // 5 DATOS A MANIPULAR
    };
  }

  constructor() {
    super();
    this.contacts = [
			{
				nombre: 'Juan Perez',
				correo: 'juanito@gmail.com',
        telefono: '5512345678',
        direccion: 'calle 1 s/n',
        genero: 'masculino'

			},
			{
				nombre: 'Maria Benitez',
				correo: 'MariBeni@gmail.com',
        telefono: '5587654321',
        direccion: 'calle 2 s/n',
        genero: 'femenino'
			},
			{
				nombre: 'Pedro Calvario',
				correo: 'petercalva@gmail.com',
        telefono: '5523456789',
        direccion: 'calle 3 s/n',
        genero: 'masculino'
			}
		];
  }

  render() {
    return html`
      <div>
        ${this.contacts.map(contact => {
          return html`
          <ul>
            <li>
              <e-contact
                name="${contact.nombre}"
                email="${contact.correo}"
              ></e-contact>
            </li>
            <li>
              <e2-contact>
                telefono="${contact.telefono}"
                direccion="${contact.direccion}"
              </e2-contact>
            </li>
            <li>
              <e3-contact>
                genero="${contact.genero}"
              </e3-contact>
            </li>
          </ul>
          `;
        })}
      </div>
    `;
  }
}

customElements.define('list-contact', ListContact)
